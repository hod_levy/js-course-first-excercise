import { v4 as uuidv4 } from 'uuid';

export class Storage {
    info;

    constructor(info) {
        this.info = info;
    }

    create(item) {
        item.id = uuidv4();
        this.info.push(item);
        return item;
    }

    find(findFunc) {
        return this.info.filter(findFunc);
    }

    getAll() {
        return this.info;
    }

    remove(findFunc) {
        this.info.sort((a, b) => findFunc(b) - findFunc(a));

        return this.info.splice(this.info.indexOf(this.info.find((element) => !findFunc(element))));
    }

    where(whereObj){
        return this.info.filter((element) => Object.entries(whereObj).every((entry) => 
            Object.entries(element).some((a) => JSON.stringify(a) === JSON.stringify(entry))));
    }
};
