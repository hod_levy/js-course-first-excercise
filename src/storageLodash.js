import _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';

export class Storage{
    info;

    constructor(info){
        this.info = info;
    }

    create(item){ 
        item.id = uuidv4();
        this.info = _.concat(this.info, item);
        return item;
    }

    find(findFunc){
        return _.filter(this.info, findFunc);
    }

    getAll(){ 
        return this.info;
    }

    remove(findFunc){ 
        return _.remove(this.info,(element) => !findFunc(element));
    }

    where(whereObj){
        return _.filter(this.info, (element) => _.every(_.entries(whereObj), (entry) => _.entries(element).some((a) => JSON.stringify(a) === JSON.stringify(entry))));
    }
};
