import * as Storage from './storage.js';
import * as Storage2 from './storageLodash.js';

const people = [
    { 
        "name": "hod",
        "age": 20
    },
    {
        "name": "Toma",
        "age": 26
    },
    { 
        "name": "Yodan",
        "age": 25
    },
    { 
        "name": "Meitar",
        "age": 19
    },
    {
        "name": "Dar",
        "age": 25
    }
];

const storage1 = new Storage.Storage(people);
const storage2 = new Storage2.Storage(people);